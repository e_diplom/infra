//SECURITY GROUP
resource "aws_security_group" "rds" {
  name        = "rds"
  vpc_id      = aws_vpc.this.id

 
  ingress {
    description = "Postgres Port"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [
        aws_subnet.priv1.cidr_block,
        aws_subnet.priv2.cidr_block
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(var.tags, var.env_prod, { Name = "csit-k-prod" })
}

resource "aws_db_subnet_group" "rds" {
  name       = "rds"
  subnet_ids = [
      aws_subnet.priv1.id,
      aws_subnet.priv2.id
  ]
  
  tags = merge(var.tags, var.env_prod, { Name = "csit-k-prod" })
}

resource "random_password" "password" {
  length = 24
  special = true
  override_special = "_%"
}

resource "aws_db_instance" "prod" {
  engine               = "postgres"
  engine_version       = "13.4"

  instance_class       = "db.t4g.micro"

  allocated_storage    = 20
  storage_type         = "gp2"

  db_name                 = "prod"
  username             = "postgres"
  password             = random_password.password.result #"rw74!nvJv4$YJwvgsSPpXCpU"

  db_subnet_group_name = aws_db_subnet_group.rds.id
  vpc_security_group_ids = [aws_security_group.rds.id]

  publicly_accessible = false
  skip_final_snapshot = true

  tags = merge(var.tags, var.env_prod, { Name = "csit-k-prod" })
}

resource "aws_db_instance" "dev" {
  engine               = "postgres"
  engine_version       = "13.4"

  instance_class       = "db.t4g.micro"

  allocated_storage    = 20
  storage_type         = "gp2"

  db_name              = "dev"
  username             = "postgres"
  password             = random_password.password.result #"rw74!nvJv4$YJwvgsSPpXCpU" #

  db_subnet_group_name = aws_db_subnet_group.rds.id
  vpc_security_group_ids = [aws_security_group.rds.id]

  publicly_accessible = false
  skip_final_snapshot = true

  tags = merge(var.tags, var.env_dev, { Name = "csit-k-dev" })
}

