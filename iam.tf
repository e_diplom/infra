data "aws_caller_identity" "current" {}

# EKS Cluster role
resource "aws_iam_role" "eks_cluster" {
  name = "csit_tf_eks_cluster"
  assume_role_policy = <<POLICY
{
 "Version": "2012-10-17",
 "Statement": [
   {
   "Effect": "Allow",
   "Principal": {
    "Service": "eks.amazonaws.com"
   },
   "Action": "sts:AssumeRole"
   }
  ]
 }
POLICY
}

resource "aws_iam_role_policy_attachment" "eks_cluster_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.eks_cluster.name}"
}

# EKS Node role
resource "aws_iam_role" "eks_nodes" {
  name = "csit_tf_eks_node_group"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "CloudWatchAgentServerPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  role       = aws_iam_role.eks_nodes.name
}

### GitLab User
resource "aws_iam_policy" "gitlab_user" {
  name = "csit-ecr-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "route53:ChangeResourceRecordSets",
          "elasticloadbalancing:DescribeLoadBalancers",
          "ecr:*",
          "eks:*",
          "cloudwatch:PutDashboard",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_user" "gitlab_user" {
  name = "csit-gitlab"
}

resource "aws_iam_user_policy_attachment" "gitlab_user_attach" {
  user       = "${aws_iam_user.gitlab_user.name}"
  policy_arn = "${aws_iam_policy.gitlab_user.arn}"
}

resource "aws_iam_user_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  user       = "${aws_iam_user.gitlab_user.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}

resource "aws_iam_access_key" "gitlab_user" {
  user    = "${aws_iam_user.gitlab_user.name}"
  depends_on = [
    aws_iam_user.gitlab_user,
  ]
  pgp_key = "mQGNBGH2bSwBDADjcRb8bX+RxQipnfDItcfqOZduUl7JRs2vnX2BBuASVn59mYUjBqKbf9wzezQqotWld0OgETk1NbAFidhAUGy0aK3NJSsL8oBf4qFX1aEQxGc2pozmy8CobJfMY/yS7OU+5ToQFcwYWBnO8c6+S2gBy2/OgtD4Chz0KQbGcYI1BuRI5YvUdM7lBbAvLaLDkRULbh6Uq/OA+ZiYn3tTKHSRFYao+dsn5zJvNGPcaWVHWISZNfXxT4jvUgOVlflvxDPYf2hrhGE1E6569L1msONChT86coevoa7/4y9sEz9Bf2VMauD/AV+itmu5/WZIJJ6H5LL9UnAeZbUpVMLQn7u83YgMO5qp5f00LtR8D3lF10rjnp5Mz/AQhwNfTMV2qRKVtBbpNfx7vDx9NYw8NWaJVca6p6c3yMC4pA5nwW9rl/JXw1yIbjsFLM+PN9ggxTkE3rAN3WNK7Fw9liTvtz7ZPoPBO+qCMom4lEkDGA5lnCahERz5zTKOJ7KSWfO59kMAEQEAAbQZQWxleCBGb3ggPGdtYnRAeWFuZGV4LnJ1PokB1AQTAQgAPhYhBGa6CMtvCr/IEHcRsLs7whxV4/gpBQJh9m0sAhsDBQkDwmcABQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJELs7whxV4/gpuZoMAJKZYIPYoMYoMdmLinhIr/hAtegQOjeM1G9aQL9D8MTpaoBFg3ntWUM4+pjPocscvVjHQ/TzcTdEzIYzy8VOI93K92ggBz6lD0JfJERAX8dFMB8Bf9gDavYVmEzH23KtYBbas+dENtoLa5jSqTrb3fpJj/NpoklntGv/eSuUpxjRNUKKmZ9B94dgUyu0IlqGl2iYgJtH9uUptCFUVWYwsNilY0Qw/4yJ7WHds9KIL3eCGprfiP44oB75I/nRIDG8XGWOkwfbEV0w+uiRaAywu/So3o/BE0C1w91MeZ/Gx0WAH0GnuX6ru6VSByeAqUtrDK9NcR6AoCstpsJEpo0dGmjLzUAaWJZ1Plh6ZNxR2F6/ZTqXO+ndNQzT27FgXLNQ/Q402uEg5Nv91PIJfusEOu2vm4AoJT/YFm6TZt22QotarhHLYbErg2gTdrGY2hz9B1r5cdWt02gVuH59lzzOEfxkk5s25RFORjzVLvaLiER/ZLlCL/g/79X8K9mBLzO+srkBjQRh9m0sAQwAlCquU070hCUeLsyys7kQctln4wcbI4C3d+DzW8UAe7FASYefd4B6eObPtsir2jj/6KDW77mj4eMfWpqKHFVKHKhadKAcQEn2wTK060crfWlVP8NgSE2HqsN+9DNncpqNMxsghs10FfuWpizv09zZF11bFaliI88T5bh9dtkJnvDietPLXyurWK66viWqtM9iH3OZayDYPrDgPBjDuscumyIneRXlEkBIvymPbbP45VZMRu1TELAyaRBFOLWhy8KgxvA5+YYZsZ4QgE0s6eAQvH5SQ0yrbTETIZSiFLpRfN66/QDPcV1423WD2jq0QOqRVfdarSfkV9Dy19oMrfztTTZNQ7rlg+UIYtdM0B2ykaEW8gwbKQ0cUfmdIzJ5V0QqKgFfLW/Sy9gLLNNOXczPwhvCkoBRkvuaPSYRrOVCcM0lK8QuA48S9yN/hVD+dAgsQz1WeIru/PzN1X75z6Ry+RhwsLSeS25Wsx+AUI4aBspOXrTTzGG6insQTDSYBBC5ABEBAAGJAbwEGAEIACYWIQRmugjLbwq/yBB3EbC7O8IcVeP4KQUCYfZtLAIbDAUJA8JnAAAKCRC7O8IcVeP4KaLgDADb7uS97qNsd9ZDgw/F4xecYpJPMxCEVZ+lQGXWVgt5S1mnpRE82TRRZBrl9d7T2/qwsH2Hz5pH62Mhvl83IthGBuFgm6GTdTYfU4BQjyXFbdC2TJubxPeCPr7LNRCzWyR1DdxNCLzJbzABU8334kLCn/DWxqH/a5+J+EUBUWbXf2vy874hnwvDl+ynLcCabUx8r5dPhzNnkleHBxy11nxRx0sTBM42ZhfDXgNMyovNWuCAEiMTr6knYANFRct9OxfhjNxCcsbK8WpptXaMW4BnPR3lVCKuq92bPMBSeco0eNb0Sxg6AelFv0YRTF4gC/Ari9eEJGQyi10IRn6ia75a62OrVNUpUioU9F1khYkzdrk6bnKSukWe6yj6hxY1Xnu5Z55tvZRJ0NF+jLj5nynFBGKjxzZs6Kapy0tBnJ8m3cBxEO2FXp0SFVl0jZ9ZGqVLRNJztaHOyaeZXHwxa/HImxYpmWse3L+TpjIsti44HUWcrjvGvUjAn4fjkASt3Xo="
}



# ALB Ingress
data "http" "alb_ingress_policy" {
  url = "https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.3.1/docs/install/iam_policy.json"
}

resource "aws_iam_policy" "alb_ingress" {
  name        = "AWSLoadBalancerControllerIAMPolicy"
  description = "Policy for alb-ingress service"

  policy = data.http.alb_ingress_policy.body
}

# Role
data "aws_iam_policy_document" "eks_oidc_assume_role" {
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_node_group.node
  ]
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"
    condition {
      test     = "StringEquals"
      variable = "${replace(aws_eks_cluster.eks_cluster.identity[0].oidc[0].issuer, "https://", "")}:sub"
      values = [
        "system:serviceaccount:kube-system:eks-alb-ingress"
      ]
    }
    principals {
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${replace(aws_eks_cluster.eks_cluster.identity[0].oidc[0].issuer, "https://", "")}"
      ]
      type = "Federated"
    }
  }
}


resource "aws_iam_role" "alb_ingress" {
  name               = "eks-alb-ingress"
  assume_role_policy = data.aws_iam_policy_document.eks_oidc_assume_role.json
}

resource "aws_iam_role_policy_attachment" "alb_ingress" {
  role       = aws_iam_role.alb_ingress.name
  policy_arn = aws_iam_policy.alb_ingress.arn
}