provider "aws" {
  region = "us-east-2"
  profile = "cs_sndbx"
}

resource "aws_security_group" "eks_cluster" {
  name        = "csit_eks_cluster"
  vpc_id      = aws_vpc.this.id


  ingress {                  # Inbound Rule
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["194.110.8.171/32"]
  }

  egress {                   # Outbound Rule
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


}

resource "aws_cloudwatch_log_group" "eks_lg" {
  name              = "/aws/eks/${var.eks_name}/cluster"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "eks_lg_cins_app" {
  name              = "/aws/containerinsights/${var.eks_name}/application"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "eks_lg_cins_plane" {
  name              = "/aws/containerinsights/${var.eks_name}/dataplane"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "eks_lg_cins_host" {
  name              = "/aws/containerinsights/${var.eks_name}/host"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "eks_lg_cins_perf" {
  name              = "/aws/containerinsights/${var.eks_name}/performance"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "eks_lg_cins_prom" {
  name              = "/aws/containerinsights/${var.eks_name}/prometheus"
  retention_in_days = 30
}

resource "aws_eks_cluster" "eks_cluster" {
  name     = var.eks_name
  role_arn = "${aws_iam_role.eks_cluster.arn}"

  vpc_config {
    security_group_ids = [aws_security_group.eks_cluster.id]
    subnet_ids         = [
      aws_subnet.pub1.id,
      aws_subnet.pub2.id,
      aws_subnet.priv1.id,
      aws_subnet.priv2.id
    ] 
  }
  
  enabled_cluster_log_types = ["api", "audit"]

  depends_on = [
    aws_iam_role.eks_cluster,
    aws_iam_role_policy_attachment.eks_cluster_AmazonEKSClusterPolicy,
    aws_security_group.eks_cluster,
    aws_cloudwatch_log_group.eks_lg
  ]

  #tags = merge(var.tags, var.env_prod, { Name = "csit_eks_cluster" })
}

resource "aws_eks_node_group" "node" {
  cluster_name    = aws_eks_cluster.eks_cluster.name
  node_group_name = "csit_node_group1"
  node_role_arn   = aws_iam_role.eks_nodes.arn
  subnet_ids      = [
    aws_subnet.priv1.id,
    aws_subnet.priv2.id
  ]
  instance_types  = ["t3a.small"]
  capacity_type   = "SPOT"

  scaling_config {
    desired_size = 3
    max_size     = 4
    min_size     = 2
  }

  depends_on = [
    aws_iam_role.eks_nodes,
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.CloudWatchAgentServerPolicy,
  ]

  tags = merge(var.tags, var.env_prod, { Name = "csit-k-prod" })
  tags_all = merge(var.tags, var.env_prod, { Name = "csit-k-prod" })
}

data "aws_eks_cluster" "eks_cluster" {
  name = aws_eks_cluster.eks_cluster.name
}

data "aws_eks_cluster_auth" "eks_cluster" {
  name = aws_eks_cluster.eks_cluster.name
}


data "tls_certificate" "eks_cluster" {
  url = aws_eks_cluster.eks_cluster.identity[0].oidc[0].issuer
}

resource "aws_iam_openid_connect_provider" "oidc_provider" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.eks_cluster.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.eks_cluster.identity[0].oidc[0].issuer
}