variable "ecr_name" {
  default = "csit-k"
}

variable "eks_name" {
  default = "csit_eks_cluster"
}

variable "tags" {
  type = map
  default = {
    Project = "edip"
    Orchestration = "Terraform"
    CreatedBy = "Alexey Bublienko"
  }
}

variable "env_prod" {
  type = map
  default = {
    Environment = "PROD"
  }
}

variable "env_dev" {
  type = map
  default = {
    Environment = "DEV"
  }
}