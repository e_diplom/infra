output "aws_iam_access_key_id" {
  value = aws_iam_access_key.gitlab_user.id
}

output "aws_iam_secret" {
  value = aws_iam_access_key.gitlab_user.encrypted_secret
}

output "ecr_url" {
  value = aws_ecr_repository.ecr.repository_url
}

output "k8s_endpoint" {
  value = aws_eks_cluster.eks_cluster.endpoint
}

output "kubeconfig_gitlab" {
  value = base64encode(local.kubeconfig_gitlab)
}

output "kubeconfig_local" {
  value = base64encode(local.kubeconfig_local)
}

#output "k8s_certificate_authority_data" {
#  value = aws_eks_cluster.eks_cluster.certificate_authority[0].data
#}
#
#output "identity-oidc-issuer" {
#  value = aws_eks_cluster.eks_cluster.identity[0].oidc[0].issuer
#}
#
#output "kubeconfig_gitlab" {
#  value = base64encode(data.template_file.kubeconfig_gitlab.rendered)
#}
#
#output "kubeconfig_local" {
#  value = base64encode(data.template_file.kubeconfig_local.rendered)
#}