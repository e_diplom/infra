locals {
  depends_on = [ aws_eks_cluster.eks_cluster ]
  
  kubeconfig_gitlab = <<-EOT
    apiVersion: v1
    clusters:
    - cluster:
        server: ${aws_eks_cluster.eks_cluster.endpoint}
        certificate-authority-data: "${aws_eks_cluster.eks_cluster.certificate_authority[0].data}"
      name: kubernetes
    contexts:
    - context:
        cluster: kubernetes
        user: gitlab
      name: aws
    current-context: aws
    kind: Config
    preferences: {}
    users:
    - name: gitlab
      user:
        exec:
          apiVersion: client.authentication.k8s.io/v1alpha1
          command: aws-iam-authenticator
          args:
            - "token"
            - "-i"
            - "${aws_eks_cluster.eks_cluster.name}"
          # env:
            # - name: AWS_PROFILE
            #   value: "aws-profile"
  EOT

  kubeconfig_local = <<-EOT
    apiVersion: v1
    clusters:
    - cluster:
        server: ${aws_eks_cluster.eks_cluster.endpoint}
        certificate-authority-data: "${aws_eks_cluster.eks_cluster.certificate_authority[0].data}"
      name: kubernetes
    contexts:
    - context:
        cluster: kubernetes
        user: aws
      name: aws
    current-context: aws
    kind: Config
    preferences: {}
    users:
    - name: aws
      user:
        exec:
          apiVersion: client.authentication.k8s.io/v1alpha1
          command: aws
          args:
            - "eks"
            - "get-token"
            - "--cluster-name"
            - "${aws_eks_cluster.eks_cluster.name}"
          # env:
            # - name: AWS_PROFILE
            #   value: "aws-profile"
  EOT
}

#data "template_file" "kubeconfig_gitlab" {
#  depends_on = [
#    aws_eks_cluster.eks_cluster,
#    aws_eks_node_group.node
#  ]
#  template = file("kubeconfig.tpl")
#  vars = {
#    kube_endpoint            = "${aws_eks_cluster.eks_cluster.endpoint}"
#    kube_certificate_auth    = "${aws_eks_cluster.eks_cluster.certificate_authority[0].data}"
#    #kube_cluster_name        = "${aws_eks_cluster.eks_cluster.name}"
#    cicd_user                = "gitlab"
#    auth_command             = "aws-iam-authenticator"
#    command_args             = <<EOT
#- "token"
#        - "-i"
#        - "${aws_eks_cluster.eks_cluster.name}"
#    EOT
#    command_env              = ""
#  }
#}

#data "template_file" "kubeconfig_local" {
#  depends_on = [
#    aws_eks_cluster.eks_cluster,
#    aws_eks_node_group.node
#  ]
#  template = file("kubeconfig.tpl")
#  vars = {
#    kube_endpoint            = "${aws_eks_cluster.eks_cluster.endpoint}"
#    kube_certificate_auth    = "${aws_eks_cluster.eks_cluster.certificate_authority[0].data}"
#    #kube_cluster_name        = "${aws_eks_cluster.eks_cluster.name}"
#    cicd_user                = "aws"
#    auth_command             = "aws"
#    command_args             = <<EOT
#- "eks"
#        - "get-token"
#        - "--cluster-name" 
#        - "${aws_eks_cluster.eks_cluster.name}"
#    EOT
#    command_env              = ""
#  }
#}
