provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks_cluster.endpoint
  cluster_ca_certificate = base64decode(aws_eks_cluster.eks_cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks_cluster.token
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.eks_cluster.endpoint
    cluster_ca_certificate = base64decode(aws_eks_cluster.eks_cluster.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.eks_cluster.token
  }
}

resource "kubernetes_namespace" "dev" {
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_node_group.node,
    aws_iam_role.eks_nodes,
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.CloudWatchAgentServerPolicy,
    aws_iam_role.eks_cluster,
    aws_iam_role_policy_attachment.eks_cluster_AmazonEKSClusterPolicy,
  ]
  metadata {
    name = "dev"
  }
}

resource "kubernetes_namespace" "prod" {
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_node_group.node,
    aws_iam_role.eks_nodes,
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.CloudWatchAgentServerPolicy,
    aws_iam_role.eks_cluster,
    aws_iam_role_policy_attachment.eks_cluster_AmazonEKSClusterPolicy,
  ]
  metadata {
    name = "prod"
  }
}

locals{
  aws_auth_configmap_yaml = <<-EOT
    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: aws-auth
      namespace: kube-system
    data:
      mapUsers: |
        - userarn: "${aws_iam_user.gitlab_user.arn}"
          username: csit-ecr-eks
          groups:
            - system:masters
  EOT
}

resource "null_resource" "patch" {
  triggers = {
    kubeconfig = "${base64encode(local.kubeconfig_local)}"
    cmd_patch  = "kubectl patch configmap/aws-auth --patch \"${local.aws_auth_configmap_yaml}\" -n kube-system --kubeconfig <(echo $KUBECONFIG | base64 --decode)"
  }

  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    environment = {
      KUBECONFIG = self.triggers.kubeconfig
    }
    command = self.triggers.cmd_patch
  }
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_node_group.node,
    local.kubeconfig_local,
  ]
}

resource "kubernetes_service_account" "eks_alb_ingress" {
  depends_on = [
      aws_eks_cluster.eks_cluster,
      aws_eks_node_group.node,
  ]
  metadata {
    name = "eks-alb-ingress"
    namespace = "kube-system"
    labels = {
      "app.kubernetes.io/component" = "controller"
      "app.kubernetes.io/name" = "aws-load-balancer-controller"      
    }
    annotations = {
      "eks.amazonaws.com/role-arn" = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/eks-alb-ingress"
    }
  }
}

resource "helm_release" "alb_ingress" {
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_node_group.node,
    aws_iam_role.eks_nodes,
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.CloudWatchAgentServerPolicy,
    aws_iam_role.eks_cluster,
    aws_iam_role_policy_attachment.eks_cluster_AmazonEKSClusterPolicy,
    kubernetes_service_account.eks_alb_ingress,
    aws_iam_role_policy_attachment.alb_ingress,
    aws_iam_role.alb_ingress,
    aws_iam_policy.alb_ingress,
    kubernetes_namespace.prod,
    kubernetes_namespace.dev,
    aws_iam_openid_connect_provider.oidc_provider,
  ]
  name             = "ingress"
  repository       = "https://aws.github.io/eks-charts"
  chart            = "aws-load-balancer-controller"
  create_namespace = false
  namespace        = "kube-system"

  set {
    name  = "clusterName"
    value = aws_eks_cluster.eks_cluster.name
  }

  set {
    name  = "serviceAccount.create"
    value = "false"
  }

  set {
    name  = "serviceAccount.name"
    value = aws_iam_role.alb_ingress.name
  }

}

resource "kubernetes_secret" "db_dev_pass" {
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_node_group.node,
    kubernetes_namespace.dev,
    aws_db_instance.dev,
  ]
  metadata {
      name      = "db-pass"
      namespace = "dev"
  }
  data = {
      dbname = "${aws_db_instance.dev.db_name}"
      dbusername = "${aws_db_instance.dev.username}"
      dbpassword = "${aws_db_instance.dev.password}"
      dbendpoint = "${aws_db_instance.dev.address}"
  }
  type = "Opaque"
}

resource "kubernetes_secret" "db_prod_pass" {
  depends_on = [
    aws_eks_cluster.eks_cluster,
    aws_eks_node_group.node,
    kubernetes_namespace.prod,
    aws_db_instance.prod,
  ]
  metadata {
      name      = "db-pass"
      namespace = "prod"
  }
  data = {
      dbname = "${aws_db_instance.prod.db_name}"
      dbusername = "${aws_db_instance.prod.username}"
      dbpassword = "${aws_db_instance.prod.password}"
      dbendpoint = "${aws_db_instance.prod.address}"
  }
  type = "Opaque"
}
