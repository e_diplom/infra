#VPC
resource "aws_vpc" "this" {
  cidr_block = "172.254.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = merge(var.tags, var.env_prod, { Name = "csit-k" })
}


# PUBLIC SUBNETS
resource "aws_subnet" "pub1" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "172.254.0.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = false

  tags = {
    "Name" = "csit-k-pub1"
    "kubernetes.io/role/elb" = 1
    "kubernetes.io/cluster/${var.eks_name}" = "shared"
  }
}

resource "aws_subnet" "pub2" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "172.254.1.0/24"
  availability_zone = "us-east-2b"
  map_public_ip_on_launch = false

  tags = {
    "Name" = "csit-k-pub2"
    "kubernetes.io/role/elb" = 1
    "kubernetes.io/cluster/${var.eks_name}" = "shared"
  }
}

# PRIVATE SUBNETS

resource "aws_subnet" "priv1" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "172.254.2.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = false

  tags = {
    "Name" = "csit-k-priv1"
    "kubernetes.io/role/internal-elb" = 1
    "kubernetes.io/cluster/${var.eks_name}" = "shared"
  }
}

resource "aws_subnet" "priv2" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "172.254.3.0/24"
  availability_zone = "us-east-2b"
  map_public_ip_on_launch = false

  tags = {
    "Name" = "csit-k-priv2"
    "kubernetes.io/role/internal-elb" = 1
    "kubernetes.io/cluster/${var.eks_name}" = "shared"
  }
}

# IGW
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.this.id
  tags = { "Name" = "csit-k-gw" }
}

# EIP
resource "aws_eip" "nat" {
  vpc = true
  tags = { "Name" = "csit-k-nat" }
}

# NAT
resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.nat.id
  subnet_id = aws_subnet.pub1.id

  tags = { "Name" = "csit-k-nat" }
  depends_on = [aws_internet_gateway.igw]
}

# PUBLIC ROUTING TABLE
resource "aws_route_table" "public_gw" {
  vpc_id = aws_vpc.this.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = { "Name" = "csit-k-public_gw" }
}

resource "aws_route_table_association" "pub1" {
  subnet_id = aws_subnet.pub1.id
  route_table_id = aws_route_table.public_gw.id
}

resource "aws_route_table_association" "pub2" {
  subnet_id = aws_subnet.pub2.id
  route_table_id = aws_route_table.public_gw.id
}

# PRIVATE ROUTING TABLE
resource "aws_route_table" "private_nat" {
  vpc_id = aws_vpc.this.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.this.id
  }

  tags = { "Name" = "csit-k-private_nat" }
}

resource "aws_route_table_association" "priv1" {
  subnet_id = aws_subnet.priv1.id
  route_table_id = aws_route_table.private_nat.id
}

resource "aws_route_table_association" "priv2" {
  subnet_id = aws_subnet.priv2.id
  route_table_id = aws_route_table.private_nat.id
}